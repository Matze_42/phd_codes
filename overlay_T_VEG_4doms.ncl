; Overlay information from 2 domains
; November 2009

; !!!!!!!!!! first activate conda !!!!!!!!!!!!!
;          conda activate ncl_stable
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
;load "./WRFUserARW.ncl"

begin

 type = "x11"
; type = "pdf"
; type = "png"
; type = "ps"

  wks = gsn_open_wks(type, "wrf_overlay_doms_4in1_T_VEG")  ; Open graphics file

  ;# coordinates of FAIR site
  lat_target = 47.316564
  lon_target = 10.970089

; 1  - NET - 'needleleaf_evergreen_temperate_tree',&
; 2  - NEB - 'needleleaf_evergreen_boreal_tree'   ,&
; 3  - NDB - 'needleleaf_deciduous_boreal_tree'   ,&
; 4  - BETr - 'broadleaf_evergreen_tropical_tree'  ,&
; 5  - BETe - 'broadleaf_evergreen_temperate_tree' ,&
; 6  - BDTr - 'broadleaf_deciduous_tropical_tree'  ,&
; 7  - BDTe - 'broadleaf_deciduous_temperate_tree' ,&
; 8  - BDB - 'broadleaf_deciduous_boreal_tree'    ,&
; 9  - SHE - 'broadleaf_evergreen_shrub'          ,&
; 10 - SHT - 'broadleaf_deciduous_temperate_shrub',&
; 11 - SHB - 'broadleaf_deciduous_boreal_shrub'   ,&
; 12 - C3A - 'c3_arctic_grass'                    ,&
; 13 - C3N - 'c3_non-arctic_grass'                ,&
; 14 - C4 - 'c4_grass'                           ,&
; 15 - CRN - 'corn'                               ,&
; 16 - WHT - 'wheat'/  

  d1 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d01_2020-06-21_12:00:00.nc","r")
  d2 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d02_2020-06-21_12:00:00.nc","r")
  d3 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d03_2020-06-21_12:00:00.nc","r")
  d4 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d04_2020-06-21_12:00:00.nc","r")
 
  i_T_VEG = 1 
  var1_a = wrf_user_getvar(d1,"T_VEG",0)
  var1 = var1_a(i_T_VEG,:,:)
  lat1 = wrf_user_getvar(d1,"XLAT_M",0)
  lon1 = wrf_user_getvar(d1,"XLONG_M",0)
  var2_a = wrf_user_getvar(d2,"T_VEG",0)
  var2 = var2_a(i_T_VEG,:,:)
  lat2 = wrf_user_getvar(d2,"XLAT_M",0)
  lon2 = wrf_user_getvar(d2,"XLONG_M",0)
  var3_a = wrf_user_getvar(d3, "T_VEG", 0)
  var3 = var3_a(i_T_VEG,:,:)
  lat3 = wrf_user_getvar(d3, "XLAT_M", 0)
  lon3 = wrf_user_getvar(d3, "XLONG_M", 0)
  var4_a = wrf_user_getvar(d4, "T_VEG", 0)
  var4 = var4_a(i_T_VEG,:,:)
  lat4 = wrf_user_getvar(d4, "XLAT_M", 0)
  lon4 = wrf_user_getvar(d4, "XLONG_M", 0)

  var1@lat2d = lat1
  var1@lon2d = lon1
  var2@lat2d = lat2
  var2@lon2d = lon2
  var3@lat2d = lat3
  var3@lon2d = lon3
  var4@lat2d = lat4
  var4@lon2d = lon4

  dom_dims = dimsizes(var1)
  dom_rank = dimsizes(dom_dims)
  nx1 = dom_dims(dom_rank - 1) - 1
  ny1 = dom_dims(dom_rank - 2) - 1
  dom_dims = dimsizes(var2)
  dom_rank = dimsizes(dom_dims)
  nx2 = dom_dims(dom_rank - 1) - 1
  ny2 = dom_dims(dom_rank - 2) - 1
  dom_dims = dimsizes(var3)
  dom_rank = dimsizes(dom_dims)
  nx3 = dom_dims(dom_rank - 1) - 1
  ny3 = dom_dims(dom_rank - 2) - 1
  dom_dims = dimsizes(var4)
  dom_rank = dimsizes(dom_dims)
  nx4 = dom_dims(dom_rank - 1) - 1
  ny4 = dom_dims(dom_rank - 2) - 1

  res                 = True

; Set some contouring resources.
  res@cnFillOn        = True
  res@cnFillPalette =   "cmocean_tempo"            ; set color map
  ;res@cnFillMode = "RasterFill"
  res@cnLinesOn       = False
  res@cnLineLabelsOn  = False
  res@cnInfoLabelOn   = False
  res@gsnSpreadColors = True
  res@gsnLeftString = ""
  res@gsnRightString = ""
  ;res@cnLevelSelectionMode = "ExplicitLevels"
  ;res@cnLevels = (/1,5,7,10,13,16,17/)
  ;res@cnLevels = (/0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16/)
  res@cnExplicitLabelBarLabelsOn = True
  res@lbLabelBarOn = True 
  ;res@lbLabelAngleF = 45.0
  res@lbLabelAutoStride = True
  ;res@lbLabelFontHeightF = 0.01 
  ;res@lbLabelAlignment = "InteriorEdges"
  res@gsnDraw         = False
  res@gsnFrame        = False

  res2 = res

; Add map resources
  res@mpDataBaseVersion     = "MediumRes"          ; Default is LowRes
  res@mpOutlineDrawOrder    = "PostDraw"           ; Draw map outlines last
  res@mpGridAndLimbOn       = False                ; Turn off lat/lon lines
  res@pmTickMarkDisplayMode = "Always"             ; Turn on map tickmarks
  res = set_mp_wrf_map_resources(d1,res)
  res@mpLimitMode        = "Corners"               ; Portion of map to zoom
  res@mpLeftCornerLatF   = lat1(0,0)
  res@mpLeftCornerLonF   = lon1(0,0)
  res@mpRightCornerLatF  = lat1(ny1,nx1)
  res@mpRightCornerLonF  = lon1(ny1,nx1)

  res3 = res
  res4 = res

; Add label bar resources
  res@lbLabelAutoStride = True
  res@gsnMaximize     = True    ; Maximize plot in frame
  res2@lbLabelBarOn = False  ; Labelbar already created in 1st plot
  res2@gsnMaximize  = False  ; Use maximization from original plot

; we need these to later draw boxes for the location of the nest domain
  xbox_out = new(5,float)
  ybox_out = new(5,float)
  lnres = True
  lnres@gsLineThicknessF  = 1.5


; make images
  map   = gsn_csm_contour_map(wks, var1, res)
  plot2 = gsn_csm_contour(wks, var2, res2)
  plot3 = gsn_csm_contour(wks, var3, res2)
  plot4 = gsn_csm_contour(wks, var4, res2)

; PLOT MAPS
  overlay(map, plot2)
  overlay(map, plot3)
  overlay(map, plot4)

  draw(map)  

; PLOT BOXES
  xbox = (/lon2(0,0),lon2(0,nx2),lon2(ny2,nx2),lon2(ny2,0),lon2(0,0)/)
  ybox = (/lat2(0,0),lat2(0,nx2),lat2(ny2,nx2),lat2(ny2,0),lat2(0,0)/)
  datatondc(map, xbox, ybox, xbox_out, ybox_out)
  gsn_polyline_ndc(wks, xbox_out, ybox_out, lnres)

  xbox3 = (/lon3(0,0),lon3(0,nx3),lon3(ny3,nx3),lon3(ny3,0),lon3(0,0)/)
  ybox3 = (/lat3(0,0),lat3(0,nx3),lat3(ny3,nx3),lat3(ny3,0),lat3(0,0)/)
  datatondc(map, xbox3, ybox3, xbox_out, ybox_out)
  gsn_polyline_ndc(wks, xbox_out, ybox_out, lnres)

  xbox4 = (/lon4(0,0),lon4(0,nx4),lon4(ny4,nx4),lon4(ny4,0),lon4(0,0)/)
  ybox4 = (/lat4(0,0),lat4(0,nx4),lat4(ny4,nx4),lat4(ny4,0),lat4(0,0)/)
  datatondc(map, xbox4, ybox4, xbox_out, ybox_out)
  gsn_polyline_ndc(wks, xbox_out, ybox_out, lnres)

  ;Marker for FAIR station
  xout = new(1,float)
  yout = new(1,float)
  datatondc(map, lon_target, lat_target, xout, yout)
  pmres                  = True
  pmres@gsMarkerIndex    = 15
  pmres@gsLineThicknessF  = 5
  pmres@gsMarkerSizeF    = 15.5
  pmres@gsMarkerColor    = "black"
  gsn_polymarker_ndc(wks, xout, yout, pmres)
  txres               =  True
  txres@txJust        = "TopLeft"
  txres@txFontHeightF =  0.01         ;-- default size is HUGE!
  gsn_text_ndc(wks,"FAIR site", xout-0.02, yout+0.025, txres)

  frame(wks)

  wks2 = gsn_open_wks(type, "wrf_overlay_doms_4in3_T_VEG")  ; Open graphics file
  res = set_mp_wrf_map_resources(d2,res)
  res@mpLimitMode        = "Corners"               ; Portion of map to zoom
  res@mpLeftCornerLatF   = lat3(0,0)
  res@mpLeftCornerLonF   = lon3(0,0)
  res@mpRightCornerLatF  = lat3(ny3,nx3)
  res@mpRightCornerLonF  = lon3(ny3,nx3)

  map3   = gsn_csm_contour_map(wks2, var3, res)
  plot4 = gsn_csm_contour(wks2, var4, res2)
  overlay(map3, plot4)
  draw(map3)  
  xbox4 = (/lon4(0,0),lon4(0,nx4),lon4(ny4,nx4),lon4(ny4,0),lon4(0,0)/)
  ybox4 = (/lat4(0,0),lat4(0,nx4),lat4(ny4,nx4),lat4(ny4,0),lat4(0,0)/)
  datatondc(map3, xbox4, ybox4, xbox_out, ybox_out)
  gsn_polyline_ndc(wks2, xbox_out, ybox_out, lnres)

  ;Marker for FAIR station
  datatondc(map3, lon_target, lat_target, xout, yout)
  gsn_polymarker_ndc(wks2, xout, yout, pmres)
  txres               =  True
  txres@txJust        = "TopLeft"
  txres@txFontHeightF =  0.01         ;-- default size is HUGE!
  gsn_text_ndc(wks2,"FAIR site", xout-0.02, yout+0.025, txres)

  frame(wks2)
end
