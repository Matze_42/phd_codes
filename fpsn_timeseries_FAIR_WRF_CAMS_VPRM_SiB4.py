#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 26 11:36:35 2021

@author: madse
"""

import netCDF4 as nc
import glob
import os
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
from datetime import datetime, timedelta


def haversine(lat1, lon1, lat2, lon2):
    R = 6371  # Earth radius in kilometers
    lat1_rad, lon1_rad = np.radians([lat1, lon1])
    lat2_rad, lon2_rad = np.radians([lat2, lon2])
    dlat = lat2_rad - lat1_rad
    dlon = lon2_rad - lon1_rad
    # Haversine formula
    a = (
        np.sin(dlat / 2) ** 2
        + np.cos(lat1_rad) * np.cos(lat2_rad) * np.sin(dlon / 2) ** 2
    )
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    distance = R * c
    return distance


def find_nearest_grid_point_and_distance(nc_fid, lat_target, lon_target):
    lat_idx = np.unravel_index(
        np.abs(nc_fid.variables["XLAT"][:] - lat_target).argmin(),
        nc_fid.variables["XLAT"][:].shape,
    )
    lon_idx = np.unravel_index(
        np.abs(nc_fid.variables["XLONG"][:] - lon_target).argmin(),
        nc_fid.variables["XLONG"][:].shape,
    )
    lat_nearest = nc_fid.variables["XLAT"][lat_idx]
    lon_nearest = nc_fid.variables["XLONG"][lon_idx]
    dist_to_CAMS = haversine(lat_target, lon_target, lat_nearest, lon_nearest)
    return dist_to_CAMS


def get_int_var(nc_fid, lat_target, lon_target, WRF_var):
    interpolated_var_dX = griddata(
        (
            nc_fid.variables["XLAT"][:].flatten(),
            nc_fid.variables["XLONG"][:].flatten(),
        ),
        nc_fid.variables[WRF_var][:].flatten(),
        (lat_target, lon_target),
        method="linear",
    )
    return interpolated_var_dX


################################# INPUT ##############################################

# coordinates of FAIR site
lat_target = 47.316564
lon_target = 10.970089
# Select start and end dates and times
subdir = "4_domains_1km_FAIR"  # 3_domains_3km_alps
month = "07"
day = "28"
hour = "15"
subsubdir = "2020_"+  month+ "_" +  day
start_date = "2020-" + month + "-" + day + "_"+hour+":00:00" # "2020-06-21 00:00:00"
datestr_ft = "2020-" + month + "-" + day + " "+hour+":00:00" 
end_date = "2020-" + month + "-" + str(int(day) + 1) + "_"+hour+":00:00" # "2020-06-22 00:00:00"

#######################################################################################
# load CAMS data
CAMS_path = (
    "/home/madse/Build_WRF/DATA/CAMS/ghg-reanalysis_lat_43_51_lon_5_17_2020"
    + month
    + ".nc"
)
CAMS_data = nc.Dataset(CAMS_path)
times_CAMS = CAMS_data.variables["time"]
# # Find the nearest grid point to the target coordinates
# lat_idx = np.abs(CAMS_data.variables["latitude"][:] - lat_target).argmin()
# lon_idx = np.abs(CAMS_data.variables["longitude"][:] - lon_target).argmin()
# get height of CAMS
CAMS_path_geop = "/home/madse/Build_WRF/DATA/CAMS/geopotential.nc"
CAMS_geop = nc.Dataset(CAMS_path_geop, "r")
lat_idx_geop = np.abs(CAMS_geop.variables["latitude"][:] - lat_target).argmin()
lon_idx_geop = np.abs(CAMS_geop.variables["longitude"][:] - lon_target).argmin()
CAMS_hgt = CAMS_geop.variables["z"][0, lat_idx_geop, lon_idx_geop] / 9.80665

# Find the interpolated height the target coordinates
lat_grid_g = CAMS_geop.variables["latitude"][:]
lon_grid_g = CAMS_geop.variables["longitude"][:]
z_grid = CAMS_geop.variables["z"][0, :, :].data / 9.80665
lat_mesh_g, lon_mesh_g = np.meshgrid(lat_grid_g, lon_grid_g, indexing="ij")
points = np.array([[lat_target, lon_target]])
z_CAMS_int = griddata(
    (lat_mesh_g.flatten(), lon_mesh_g.flatten()),
    z_grid.flatten(),
    points,
    method="linear",
)
lat_nearest = CAMS_geop.variables["latitude"][lat_idx_geop]
lon_nearest = CAMS_geop.variables["longitude"][lon_idx_geop]
# Assuming lat_target, lon_target, lat_nearest, lon_nearest are given
dist_to_CAMS = haversine(lat_target, lon_target, lat_nearest, lon_nearest)

CAMS_vars = ["fco2nee", "fco2gpp", "fco2rec", "t2m"]

factor_kgC = 1000 / 44 * 1000000  # conversion from kgCO2/m2/s to  mumol/m2/s
factors = [-factor_kgC, factor_kgC, -factor_kgC, 1]

# WRF output data
directory_path = "/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/"+subdir+"/"+subsubdir+"/"
# Use glob to list all files in the directory
file_list_d1 = sorted(glob.glob(os.path.join(directory_path, "wrfout_d01*")))
file_list_d2 = sorted(glob.glob(os.path.join(directory_path, "wrfout_d02*")))
file_list_d3 = sorted(glob.glob(os.path.join(directory_path, "wrfout_d03*")))
file_list_d4 = sorted(glob.glob(os.path.join(directory_path, "wrfout_d04*")))
# WRF_vars = ["FCO2"]  # "T_VEG",
WRF_vars = ["FCO2", "FPSN", "RECO", "T2"]  # "T_VEG",
units = [" [mmol m² s⁻¹]", " [mmol m² s⁻¹]", " [mmol m² s⁻¹]", " [K]"]
name_vars = {"FCO2": "WRF NEE", "FPSN": "WRF GPP", "RECO": "WRF RECO", "T2": "WRF T2M"}

parsed_date = datetime.strptime(datestr_ft, "%Y-%m-%d %H:%M:%S")
formatted_date = parsed_date.strftime("%Y_%m_%d")

CTE_HR_flags = [True, False, False, False]  # ["nep"]
VPRM_flags = [True, True, True, False]
VPRM_vars = ["NEE", "GEE", "RESP", "NaN"]
VPRM_factors = [1, -1, 1, 1]
flag_ini = True

for (
    WRF_var,
    CAMS_var,
    factor,
    unit,
    CTE_HR_flag,
    VPRM_flag,
    VPRM_var,
    VPRM_factor,
) in zip(
    WRF_vars,
    CAMS_vars,
    factors,
    units,
    CTE_HR_flags,
    VPRM_flags,
    VPRM_vars,
    VPRM_factors,
):
    figname = ( "../plotting/"+
        "FAIR_ts_WRF_CAMS_VPRM_SiB4_" + name_vars[WRF_var].replace(' ', '_')+ "_" + datestr_ft + ".eps"
    )
    figname_diff = (
        "../plotting/"
        + "FAIR_ts_diff_WRF_CAMS_VPRM_SiB4_"
        + name_vars[WRF_var].replace(' ', '_')
        + "_" + datestr_ft
        + ".eps"
    )
    # CTE_HR:
    if CTE_HR_flag:
        file_path_CTE_HR = "/home/madse/Build_WRF/DATA/CTE_HR/nep.2020"+month+".nc"
        CTE_HR_data = nc.Dataset(file_path_CTE_HR)
        times_CTE_HR = CTE_HR_data.variables["time"]

        if flag_ini:
            lat_idx = np.abs(CTE_HR_data.variables["latitude"][:] - lat_target).argmin()
            lon_idx = np.abs(
                CTE_HR_data.variables["longitude"][:] - lon_target
            ).argmin()
            lat_nearest = CTE_HR_data.variables["latitude"][lat_idx]
            lon_nearest = CTE_HR_data.variables["longitude"][lon_idx]
            # Assuming lat_target, lon_target, lat_nearest, lon_nearest are given
            dist_to_CTE_HR = haversine(lat_target, lon_target, lat_nearest, lon_nearest)
            grid_size_x_CTE_HR = haversine(
                CTE_HR_data.variables["latitude"][0],
                CTE_HR_data.variables["longitude"][0],
                CTE_HR_data.variables["latitude"][0],
                CTE_HR_data.variables["longitude"][1],
            )
            grid_size_y_CTE_HR = haversine(
                CTE_HR_data.variables["latitude"][0],
                CTE_HR_data.variables["longitude"][0],
                CTE_HR_data.variables["latitude"][1],
                CTE_HR_data.variables["longitude"][0],
            )
            print(
                "CTE_HR grid size is for lon:",
                grid_size_x_CTE_HR,
                " and lat ",
                grid_size_y_CTE_HR,
            )

    # VPRM:
    if VPRM_flag:
        file_path_VPRM = (
            "/media/madse/scratch/VPRM_data/VPRM_ECMWF_" + VPRM_var + "_2020_CP.nc"
        )
        VPRM_data = nc.Dataset(file_path_VPRM)
        times_VPRM = VPRM_data.variables["time"]
        if flag_ini:
            lat_idx = np.abs(VPRM_data.variables["lat"][:] - lat_target).argmin()
            lon_idx = np.abs(VPRM_data.variables["lon"][:] - lon_target).argmin()
            lat_nearest = VPRM_data.variables["lat"][lat_idx]
            lon_nearest = VPRM_data.variables["lon"][lon_idx]
            # Assuming lat_target, lon_target, lat_nearest, lon_nearest are given
            dist_to_VPRM = haversine(lat_target, lon_target, lat_nearest, lon_nearest)
            grid_size_x_VPRM = haversine(
                VPRM_data.variables["lat"][0],
                VPRM_data.variables["lon"][0],
                VPRM_data.variables["lat"][0],
                VPRM_data.variables["lon"][1],
            )
            grid_size_y_VPRM = haversine(
                VPRM_data.variables["lat"][0],
                VPRM_data.variables["lon"][0],
                VPRM_data.variables["lat"][1],
                VPRM_data.variables["lon"][0],
            )
            print(
                "VPRM grid size is for lon:",
                grid_size_x_VPRM,
                " and lat ",
                grid_size_y_VPRM,
            )

    # initializing varables
    var_x_d4 = []
    var_x_d3 = []
    var_x_d2 = []
    var_x_d1 = []
    CAMS_data_out = []
    CTE_HR_data_out = []
    VPRM_data_out = []
    time_steps = []
    i = 0
    flag_ini = False
    # Loop through the files for the 24-hour period
    for nc_f1, nc_f2, nc_f3, nc_f4 in zip(
        file_list_d1, file_list_d2, file_list_d3, file_list_d4
    ):
        nc_fid4 = nc.Dataset(nc_f4, "r")
        nc_fid3 = nc.Dataset(nc_f3, "r")
        nc_fid2 = nc.Dataset(nc_f2, "r")
        nc_fid1 = nc.Dataset(nc_f1, "r")

        times_variable = nc_fid4.variables["Times"]
        start_date_bytes = times_variable[0, :].tobytes()
        start_date_str = start_date_bytes.decode("utf-8")
        print("Processing WRF Date:", start_date_str)
        print("... ")

        # process CAMS data if times fit
        start_date_nc_f4 = datetime.strptime(start_date_str, "%Y-%m-%d_%H:%M:%S")
        j = 0
        for time_CAMS in times_CAMS:
            date_CAMS = datetime(1900, 1, 1) + timedelta(hours=int(time_CAMS))
            j = j + 1
            if start_date_nc_f4 == date_CAMS:
                # CAMS
                lat_grid = CAMS_data.variables["latitude"][:]
                lon_grid = CAMS_data.variables["longitude"][:]
                var_grid = (
                    CAMS_data.variables[CAMS_var][j - 1, :, :].data * factor
                )  # convert unit to mmol m-2 s-1
                lat_mesh, lon_mesh = np.meshgrid(lat_grid, lon_grid, indexing="ij")
                points = np.array([[lat_target, lon_target]])
                CAMS_data_int = griddata(
                    (lat_mesh.flatten(), lon_mesh.flatten()),
                    var_grid.flatten(),
                    points,
                    method="linear",
                )
                CAMS_data_out.extend(CAMS_data_int.tolist())
                print("CAMS date matches: interpolating CAMS data")
                print("...")
        # CTE_HR: times_CTE_HR
        if CTE_HR_flag:
            j = 0
            for time_CTE_HR in times_CTE_HR:
                date_CTE_HR = datetime(2000, 1, 1) + timedelta(seconds=int(time_CTE_HR))
                j = j + 1
                if start_date_nc_f4 == date_CTE_HR:
                    lat_grid = CTE_HR_data.variables["latitude"][:]
                    lon_grid = CTE_HR_data.variables["longitude"][:]
                    var_grid = (
                        CTE_HR_data.variables["nep"][j - 1, :, :].data * 10**6
                    )  # convert mol m-2 s-1 to mumol m-2 s-1
                    lat_mesh, lon_mesh = np.meshgrid(lat_grid, lon_grid, indexing="ij")
                    points = np.array([[lat_target, lon_target]])
                    CTE_HR_data_int = griddata(
                        (lat_mesh.flatten(), lon_mesh.flatten()),
                        var_grid.flatten(),
                        points,
                        method="linear",
                    )
                    CTE_HR_data_out.append(CTE_HR_data_int.tolist())
                    print("interpolating CTE_HR data")
                    print("... at ", date_CTE_HR)

        if VPRM_flag:
            j = 0
            for time_VPRM in times_VPRM:
                date_VPRM = datetime(2020, 1, 1) + timedelta(hours=int(time_VPRM))
                j = j + 1
                if start_date_nc_f4 == date_VPRM:
                    lat_grid = VPRM_data.variables["lat"][:]
                    lon_grid = VPRM_data.variables["lon"][:]
                    var_grid = (
                        VPRM_data.variables[VPRM_var][j - 1, :, :].data * VPRM_factor
                    )  # unit is mmol m-2 s-1
                    lat_mesh, lon_mesh = np.meshgrid(lat_grid, lon_grid, indexing="ij")
                    points = np.array([[lat_target, lon_target]])
                    VPRM_data_int = griddata(
                        (lat_mesh.flatten(), lon_mesh.flatten()),
                        var_grid.flatten(),
                        points,
                        method="linear",
                    )
                    VPRM_data_out.append(VPRM_data_int.tolist())
                    print("interpolating VPRM data")
                    print("... at", date_VPRM)

        if i == 0:
            dist_to_d04 = find_nearest_grid_point_and_distance(
                nc_fid4, lat_target, lon_target
            )
            dist_to_d03 = find_nearest_grid_point_and_distance(
                nc_fid3, lat_target, lon_target
            )
            dist_to_d02 = find_nearest_grid_point_and_distance(
                nc_fid2, lat_target, lon_target
            )
            dist_to_d01 = find_nearest_grid_point_and_distance(
                nc_fid1, lat_target, lon_target
            )
            z_int_d4 = get_int_var(nc_fid4, lat_target, lon_target, "HGT")
            z_int_d3 = get_int_var(nc_fid3, lat_target, lon_target, "HGT")
            z_int_d2 = get_int_var(nc_fid2, lat_target, lon_target, "HGT")
            z_int_d1 = get_int_var(nc_fid1, lat_target, lon_target, "HGT")
            print("distance from target to nearest grid point in km: ")
            print(f"d04: {dist_to_d04:.2f}")
            print(f"d03: {dist_to_d03:.2f}")
            print(f"d02: {dist_to_d02:.2f}")
            print(f"d01: {dist_to_d01:.2f}")
            print(f"CAMS: {dist_to_CAMS:.2f}")
            print(f"CTE_HR: {dist_to_CTE_HR:.2f}")
            print(f"VPRM: {dist_to_VPRM:.2f}")
            var_x_d4.append(np.nan)
            var_x_d3.append(np.nan)
            var_x_d2.append(np.nan)
            var_x_d1.append(np.nan)
        else:
            # Interpolate values to the target coordinates
            interpolated_var_d4 = get_int_var(nc_fid4, lat_target, lon_target, WRF_var)
            var_x_d4.append(interpolated_var_d4.tolist())
            interpolated_var_d3 = get_int_var(nc_fid3, lat_target, lon_target, WRF_var)
            var_x_d3.append(interpolated_var_d3.tolist())
            interpolated_var_d2 = get_int_var(nc_fid2, lat_target, lon_target, WRF_var)
            var_x_d2.append(interpolated_var_d2.tolist())
            interpolated_var_d1 = get_int_var(nc_fid1, lat_target, lon_target, WRF_var)
            var_x_d1.append(interpolated_var_d1.tolist())

        time_step = nc_fid1.variables["Times"][:].tobytes().decode("utf-8")
        time_steps.append(time_step)
        i += 1
        # close the NetCDF files when done
        nc_fid4.close()
        nc_fid3.close()
        nc_fid2.close()
        nc_fid1.close()

    # Create a time series plot
    plt.figure(figsize=(12, 6))
    plt.plot(
        time_steps,
        var_x_d4,
        label="d04 (dx=1km hgt=" + str(int(z_int_d4)) + "m)",
    )
    plt.plot(
        time_steps,
        var_x_d3,
        label="d03 (dx=3km hgt=" + str(int(z_int_d3)) + "m)",
    )
    plt.plot(
        time_steps,
        var_x_d2,
        label="d02 (dx=9km hgt=" + str(int(z_int_d2)) + "m)",
    )
    plt.plot(
        time_steps,
        var_x_d1,
        label="d01 (dx=27km hgt=" + str(int(z_int_d1)) + "m)",
    )
    plt.plot(
        time_steps[0::3],
        CAMS_data_out,
        ls="--",
        label="CAMS (dx~60km hgt=" + str(int(z_CAMS_int)) + "m)",
    )
    if CTE_HR_flag:
        plt.plot(
            time_steps,
            CTE_HR_data_out,
            ls="--",
            label="CTE_HR (dx~10 x 20km)",
        )
    if VPRM_flag:
        plt.plot(
            time_steps,
            VPRM_data_out,
            ls="--",
            label="VPRM (dx~10km)",
        )

    plt.xlabel("Time Steps")
    plt.ylabel(unit)
    plt.title(name_vars[WRF_var] + " interpolated to FAIR site ")
    plt.xticks(rotation=90)
    plt.legend()
    plt.tight_layout()
    # plt.show()
    plt.savefig(figname, format="eps")

    # difference3 = [b - a for a, b in zip(var_x_d4, var_x_d3)]
    # difference2 = [b - a for a, b in zip(var_x_d4, var_x_d2)]
    # difference1 = [b - a for a, b in zip(var_x_d4, var_x_d1)]
    # diff_WRF_CAMS = [b - a for a, b in zip(var_x_d4[0::3], CAMS_data_out)]

    # # Create a time series plot
    # plt.figure(figsize=(12, 6))
    # plt.plot(
    #     time_steps,
    #     difference3,
    #     label="d03 (dx=3km) - d04 (dx=1km) dz=" + str(int(z_int_d3 - z_int_d4)) + "m)",
    # )
    # plt.plot(
    #     time_steps,
    #     difference2,
    #     label="d02 (dx=9km) - d04 (dx=1km) dz=" + str(int(z_int_d2 - z_int_d4)) + "m)",
    # )
    # plt.plot(
    #     time_steps,
    #     difference1,
    #     label="d01 (dx=27km) - d04 (dx=1km) dz=" + str(int(z_int_d1 - z_int_d4)) + "m)",
    # )
    # plt.plot(
    #     time_steps[0::3],
    #     diff_WRF_CAMS,
    #     label="CAMS (dx=0.75°~60km) - d04 (dx=1km) dz="
    #     + str(int(z_CAMS_int - z_int_d4))
    #     + "m)",
    # )
    # plt.xlabel("Time Steps")
    # plt.ylabel(unit)
    # plt.title(
    #     name_vars[WRF_var]
    #     + " and CAMS "
    #     + CAMS_var
    #     + " differences to d04 at FAIR site "
    # )
    # plt.xticks(rotation=90)
    # plt.legend()
    # plt.tight_layout()
    # # plt.show()
    # plt.savefig(figname_diff, format="eps")
