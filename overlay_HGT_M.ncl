; Overlay information from 2 domains
; November 2009

; !!!!!!!!!! first activate conda !!!!!!!!!!!!!
;          conda activate ncl_stable
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
;load "./WRFUserARW.ncl"

begin

 type = "x11"
; type = "pdf"
 ;type = "ps"

  wks = gsn_open_wks(type, "wrf_overlay_doms_2in1_HGT_M")  ; Open graphics file

  d1 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d01_2020-06-21_12:00:00.nc","r")
  d2 = addfile("/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/2020_06_21_4_domains/wrfout_d02_2020-06-21_12:00:00.nc","r")
  

  var1 = wrf_user_getvar(d1,"HGT_M",0)
  lat1 = wrf_user_getvar(d1,"XLAT_M",0)
  lon1 = wrf_user_getvar(d1,"XLONG_M",0)
  var2 = wrf_user_getvar(d2,"HGT_M",0)
  lat2 = wrf_user_getvar(d2,"XLAT_M",0)
  lon2 = wrf_user_getvar(d2,"XLONG_M",0)

  var1@lat2d = lat1
  var1@lon2d = lon1
  var2@lat2d = lat2
  var2@lon2d = lon2

  dom_dims = dimsizes(var1)
  dom_rank = dimsizes(dom_dims)
  nx1 = dom_dims(dom_rank - 1) - 1
  ny1 = dom_dims(dom_rank - 2) - 1
  dom_dims = dimsizes(var2)
  dom_rank = dimsizes(dom_dims)
  nx2 = dom_dims(dom_rank - 1) - 1
  ny2 = dom_dims(dom_rank - 2) - 1


  res                 = True

; Set some contouring resources.
  res@cnFillOn        = True
  res@cnLinesOn       = False
  res@cnLineLabelsOn  = False
  res@cnInfoLabelOn   = False
  res@gsnSpreadColors = True
  ;res@cnLevelSelectionMode = "ExplicitLevels"
  ;res@cnLevels             = (/200,250,300,350,400,450,500,550,600,650,700,750, \
  ;                              800,850,900,950,1000,1050,1100,1150,1200/)
  res@gsnLeftString = ""
  res@gsnRightString = ""

  res@gsnDraw         = False
  res@gsnFrame        = False

  res2 = res

; Add map resources
  res@mpDataBaseVersion     = "MediumRes"          ; Default is LowRes
  res@mpOutlineDrawOrder    = "PostDraw"           ; Draw map outlines last
  res@mpGridAndLimbOn       = False                ; Turn off lat/lon lines
  res@pmTickMarkDisplayMode = "Always"             ; Turn on map tickmarks
  res = set_mp_wrf_map_resources(d1,res)
  res@mpLimitMode        = "Corners"               ; Portion of map to zoom
  res@mpLeftCornerLatF   = lat1(0,0)
  res@mpLeftCornerLonF   = lon1(0,0)
  res@mpRightCornerLatF  = lat1(ny1,nx1)
  res@mpRightCornerLonF  = lon1(ny1,nx1)

; Add label bar resources
  res@lbLabelAutoStride = True
  res@gsnMaximize     = True    ; Maximize plot in frame


  res2@lbLabelBarOn = False  ; Labelbar already created in 1st plot
  res2@gsnMaximize  = False  ; Use maximization from original plot

; we need these to later draw boxes for the location of the nest domain
  xbox_out = new(5,float)
  ybox_out = new(5,float)
  lnres = True
  lnres@gsLineThicknessF  = 1.5

; make images
  map = gsn_csm_contour_map(wks, var1, res)
  plot = gsn_csm_contour(wks, var2, res2)


; let's make 3 plots
; 1 - the mother domain only
; 2 - the mother and nest domains
; 3 - same as 2, but add a box around domain 2



  overlay (map, plot)
  draw(map)  
  xbox = (/lon2(0,0),lon2(0,nx2),lon2(ny2,nx2),lon2(ny2,0),lon2(0,0)/)
  ybox = (/lat2(0,0),lat2(0,nx2),lat2(ny2,nx2),lat2(ny2,0),lat2(0,0)/)
  datatondc(map, xbox, ybox, xbox_out, ybox_out)
  gsn_polyline_ndc(wks, xbox_out, ybox_out, lnres)
  frame(wks)

end
