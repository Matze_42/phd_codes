#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 26 11:36:35 2021

@author: madse
"""

import netCDF4 as nc
import glob
import os
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
from datetime import datetime, timedelta


def haversine(lat1, lon1, lat2, lon2):
    """
    Calculate the Haversine distance between two points on the earth (specified in latitude and longitude).

    This function computes the great-circle distance between two points on the earth's surface using
    the Haversine formula. The distance is returned in kilometers.

    Parameters:
    -----------
    lat1 : float
        Latitude of the first point in degrees.
    lon1 : float
        Longitude of the first point in degrees.
    lat2 : float
        Latitude of the second point in degrees.
    lon2 : float
        Longitude of the second point in degrees.

    Returns:
    --------
    distance : float
        The Haversine distance between the two points in kilometers.
    """
    R = 6371  # Earth radius in kilometers
    lat1_rad, lon1_rad = np.radians([lat1, lon1])
    lat2_rad, lon2_rad = np.radians([lat2, lon2])
    dlat = lat2_rad - lat1_rad
    dlon = lon2_rad - lon1_rad
    # Haversine formula
    a = (
        np.sin(dlat / 2) ** 2
        + np.cos(lat1_rad) * np.cos(lat2_rad) * np.sin(dlon / 2) ** 2
    )
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    distance = R * c
    return distance


def find_nearest_grid_point_and_distance(nc_fid, lat_target, lon_target):
        """
    Find the nearest grid point to a given latitude and longitude in the WRF model output
    and calculate the distance to that grid point.

    Parameters:
    ----------
    nc_fid : netCDF4.Dataset
        An open netCDF4 dataset object containing the WRF model output.
    lat_target : float
        The target latitude for which to find the nearest grid point.
    lon_target : float
        The target longitude for which to find the nearest grid point.

    Returns:
    -------
    float
        The distance (in kilometers) from the target coordinates to the nearest grid point.

    Notes:
    ------
    This function uses the Haversine formula to calculate the distance between two points
    on the Earth's surface given their latitudes and longitudes.

    The function assumes that the WRF model output contains variables "XLAT" and "XLONG"
    with the latitude and longitude coordinates of the grid points.
    """
    # Find the index of the latitude grid point closest to the target latitude
    lat_idx = np.unravel_index(
        np.abs(nc_fid.variables["XLAT"][:] - lat_target).argmin(),
        nc_fid.variables["XLAT"][:].shape,
    )
    # Find the index of the longitude grid point closest to the target longitude
    lon_idx = np.unravel_index(
        np.abs(nc_fid.variables["XLONG"][:] - lon_target).argmin(),
        nc_fid.variables["XLONG"][:].shape,
    )
    # Get the latitude of the nearest grid point
    lat_nearest = nc_fid.variables["XLAT"][lat_idx]
    # Get the longitude of the nearest grid point
    lon_nearest = nc_fid.variables["XLONG"][lon_idx]
    # Calculate the Haversine distance between the target point and the nearest grid point
    dist_to_CAMS = haversine(lat_target, lon_target, lat_nearest, lon_nearest)
    # Return the calculated distance
    return dist_to_CAMS


def get_int_var(nc_fid, lat_target, lon_target, WRF_var):
    """
    Interpolates a variable from a NetCDF file at a specified latitude and longitude using linear interpolation.

    This function takes a NetCDF dataset, target latitude and longitude coordinates, and the name of a variable to be interpolated.
    It returns the interpolated value of the specified variable at the given coordinates.

    Parameters:
    -----------
    nc_fid : netCDF4.Dataset
        The NetCDF dataset containing the variable to be interpolated.
    lat_target : float
        The target latitude for interpolation.
    lon_target : float
        The target longitude for interpolation.
    WRF_var : str
        The name of the variable to be interpolated.

    Returns:
    --------
    interpolated_var_dX : float
        The interpolated value of the specified variable at the target coordinates.
    """
    interpolated_var_dX = griddata(
        (
            nc_fid.variables["XLAT"][:].flatten(),
            nc_fid.variables["XLONG"][:].flatten(),
        ),
        nc_fid.variables[WRF_var][:].flatten(),
        (lat_target, lon_target),
        method="linear",
    )
    return interpolated_var_dX


def proj_on_finer_WRF_grid(
    lats_coarse, lons_coarse, var_coarse, lats_fine, lons_fine, WRF_var_d3
):
    """
    Projects coarse grid data onto a finer WRF grid using nearest neighbor interpolation.

    This function takes coarse grid latitudes, longitudes, and variable data, along with
    fine grid latitudes and longitudes, and projects the coarse data onto the finer grid.
    The projection is done using nearest neighbor interpolation.

    Parameters:
    -----------
    lats_coarse : array-like
        Latitudes of the coarse grid.
    lons_coarse : array-like
        Longitudes of the coarse grid.
    var_coarse : array-like
        Data values on the coarse grid.
    lats_fine : array-like
        Latitudes of the fine grid.
    lons_fine : array-like
        Longitudes of the fine grid.
    WRF_var_d3 : array-like
        WRF variable data on the fine grid, used to determine the shape of the output array.

    Returns:
    --------
    proj_var : array-like
        The projected variable data on the fine grid.
    """
    proj_var = griddata(
        (lats_coarse.flatten(), lons_coarse.flatten()),
        var_coarse.flatten(),
        (lats_fine, lons_fine),
        method="nearest",
    ).reshape(WRF_var_d3.shape)
    return proj_var


def proj_CAMS_on_WRF_grid(
    lats_coarse, lons_coarse, var_coarse, lats_fine, lons_fine, WRF_var_d3
):
    """
    Projects CAMS data onto a finer WRF grid using nearest neighbor interpolation.

    This function takes coarse grid latitudes, longitudes, and variable data from CAMS,
    along with fine grid latitudes and longitudes from WRF, and projects the CAMS data
    onto the finer WRF grid. The projection is done using nearest neighbor interpolation.

    Parameters:
    -----------
    lats_coarse : array-like
        Latitudes of the coarse grid (CAMS).
    lons_coarse : array-like
        Longitudes of the coarse grid (CAMS).
    var_coarse : array-like
        Data values on the coarse grid (CAMS).
    lats_fine : array-like
        Latitudes of the fine grid (WRF).
    lons_fine : array-like
        Longitudes of the fine grid (WRF).
    WRF_var_d3 : array-like
        WRF variable data on the fine grid, used to determine the shape of the output array.

    Returns:
    --------
    proj_var : array-like
        The projected variable data from CAMS onto the finer WRF grid.
    """
    # Corrected meshgrid order
    lats_coarse_2d, lons_coarse_2d = np.meshgrid(lats_coarse, lons_coarse)
    # Reverse the order of latitude coordinates
    lat_CAMS_2d_reversed = lats_coarse_2d[::-1]
    # Reverse the order of the variable values
    var_coarse_reversed = np.flipud(var_coarse)
    # Flatten the coordinates
    points_coarse = np.column_stack(
        (lat_CAMS_2d_reversed.flatten(), lons_coarse_2d.flatten())
    )
    points_fine = np.column_stack((lats_fine.flatten(), lons_fine.flatten()))
    # Perform interpolation
    proj_var = griddata(
        points_coarse, var_coarse_reversed.flatten(), points_fine, method="nearest"
    ).reshape(WRF_var_d3.shape)

    return proj_var


def proj_on_WRF_grid(
    lats_coarse, lons_coarse, var_coarse, lats_fine, lons_fine, WRF_var_d3
):
    """
    Projects coarse grid data onto a finer WRF grid using nearest neighbor interpolation.

    This function takes coarse grid latitudes, longitudes, and variable data, along with
    fine grid latitudes and longitudes, and projects the coarse data onto the finer grid.
    The projection is done using nearest neighbor interpolation.

    Parameters:
    -----------
    lats_coarse : array-like
        Latitudes of the coarse grid.
    lons_coarse : array-like
        Longitudes of the coarse grid.
    var_coarse : array-like
        Data values on the coarse grid.
    lats_fine : array-like
        Latitudes of the fine grid.
    lons_fine : array-like
        Longitudes of the fine grid.
    WRF_var_d3 : array-like
        WRF variable data on the fine grid, used to determine the shape of the output array.

    Returns:
    --------
    proj_var : array-like
        The projected variable data on the fine grid.
    """
    # Corrected meshgrid order
    lats_coarse_2d, lons_coarse_2d = np.meshgrid(lats_coarse, lons_coarse)
    # Flatten the coordinates
    points_coarse = np.column_stack(
        (lats_coarse_2d.flatten(), lons_coarse_2d.flatten())
    )
    points_fine = np.column_stack((lats_fine.flatten(), lons_fine.flatten()))
    # Perform interpolation
    proj_var = griddata(
        points_coarse, var_coarse.flatten(), points_fine, method="nearest"
    ).reshape(WRF_var_d3.shape)

    return proj_var


################################# INPUT ##############################################

# coordinates of FAIR site
lat_target = 47.316564
lon_target = 10.970089

# set standard deviation of topography
STD_VAL = 0
VEGFRA_percentage = 10
T_bin_size = 2

# set True if you want plots
plotting_refs = True
plotting_scatter = True
plotting_scatter_all = True
T_bin_flag = True

D03 = "d04"
D02 = "d03"
D01 = "d02"

subdir = "4_domains_1km_FAIR"  # 3_domains_3km_alps
timedelt = 0
month = "07"
day = "28"
timestr = "2020-" + month + "-" + day + "_15:00:00"


#######################################################################################
# load CAMS data
CAMS_path = (
    "/home/madse/Build_WRF/DATA/CAMS/ghg-reanalysis_lat_43_51_lon_5_17_2020"
    + month
    + ".nc"
)
CAMS_data = nc.Dataset(CAMS_path)
times_CAMS = CAMS_data.variables["time"]

CAMS_vars = ["fco2nee", "fco2gpp", "fco2rec", "t2m"]
factor_kgC = 1000 / 44 * 1000000  # conversion from kgCO2/m2/s to  mumol/m2/s
factors = [-factor_kgC, factor_kgC, -factor_kgC, 1]


# WRF output data
WRF_d03 = (
    "/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/"
    + subdir
    + "/wrfout_"
    + D03
    + "_"
    + timestr
)
WRF_d02 = (
    "/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/"
    + subdir
    + "/wrfout_"
    + D02
    + "_"
    + timestr
)
WRF_d01 = (
    "/home/madse/Build_WRF/DATA/WRFOUT/Mieming_2020/"
    + subdir
    + "/wrfout_"
    + D01
    + "_"
    + timestr
)

# Use glob to list all files in the directory
# file_list_d1 = sorted(glob.glob(os.path.join(WRF_path, "wrfout_d01*")))
WRF_vars = ["FCO2", "FPSN", "RECO"]  # , "TSK"]
units = [" [mmol m² s⁻¹]", " [mmol m² s⁻¹]", " [mmol m² s⁻¹]", " [K]"]
name_vars = {"FCO2": "WRF NEE", "FPSN": "WRF GPP", "RECO": "WRF RECO", "TSK": "WRF TSK"}

CTE_HR_flags = [True, False, False, False]  # ["nep"]
VPRM_flags = [True, True, True, False]
VPRM_vars = ["NEE", "GEE", "RESP", "NaN"]
VPRM_factors = [1, -1, 1, 1]
flag_ini = True

for (
    WRF_var,
    CAMS_var,
    factor,
    unit,
    CTE_HR_flag,
    VPRM_flag,
    VPRM_var,
    VPRM_factor,
) in zip(
    WRF_vars,
    CAMS_vars,
    factors,
    units,
    CTE_HR_flags,
    VPRM_flags,
    VPRM_vars,
    VPRM_factors,
):
    # WRF
    CAMS_mean = []
    time_steps = []
    i = 0
    # Loop through the files for the period
    # for nc_f1 in file_list_d1:
    nc_fid3 = nc.Dataset(WRF_d03, "r")
    nc_fid2 = nc.Dataset(WRF_d02, "r")
    nc_fid1 = nc.Dataset(WRF_d01, "r")

    times_variable = nc_fid3.variables["Times"]
    start_date_bytes = times_variable[0, :].tobytes()
    start_date_str = start_date_bytes.decode("utf-8")
    print("Processing WRF Date:", start_date_str)
    print("... ")
    lats_fine = nc_fid3.variables["XLAT"][0, :, :]
    lons_fine = nc_fid3.variables["XLONG"][0, :, :]
    landmask = nc_fid3.variables["LANDMASK"][0, :, :]
    land_mask = landmask == 1

    stdh_mask = nc_fid3.variables["VAR"][0, :, :]
    stdh_mask = stdh_mask > STD_VAL
    mask = land_mask * stdh_mask

    WRF_var_d3 = nc_fid3.variables[WRF_var][0, :, :]
    WRF_var_d3[landmask == 0] = np.nan
    WRF_var_d3_topo = np.ma.masked_where(~mask, WRF_var_d3)

    WRF_var_d3_topo_m = np.nanmean(WRF_var_d3_topo)

    lats_d2 = nc_fid2.variables["XLAT"][0, :, :]
    lons_d2 = nc_fid2.variables["XLONG"][0, :, :]
    WRF_var_d2 = nc_fid2.variables[WRF_var][0, :, :]
    landmask_2 = nc_fid2.variables["LANDMASK"][0, :, :]
    WRF_var_d2[landmask_2 == 0] = np.nan

    proj_WRF_var_d2 = proj_on_finer_WRF_grid(
        lats_d2, lons_d2, WRF_var_d2, lats_fine, lons_fine, WRF_var_d3
    )
    WRF_var_d2_topo = np.nanmean(np.ma.masked_where(~mask, proj_WRF_var_d2))

    lats_d1 = nc_fid1.variables["XLAT"][0, :, :]
    lons_d1 = nc_fid1.variables["XLONG"][0, :, :]
    WRF_var_d1 = nc_fid1.variables[WRF_var][0, :, :]
    landmask_1 = nc_fid1.variables["LANDMASK"][0, :, :]
    WRF_var_d1[landmask_1 == 0] = np.nan

    model_TSK_d1 = nc_fid1.variables["TSK"][:]

    proj_WRF_var_d1 = proj_on_finer_WRF_grid(
        lats_d1, lons_d1, WRF_var_d1, lats_fine, lons_fine, WRF_var_d3
    )
    WRF_var_d1_topo = np.nanmean(np.ma.masked_where(~mask, proj_WRF_var_d1))

    # CTE_HR:
    if CTE_HR_flag:
        file_path_CTE_HR = "/home/madse/Build_WRF/DATA/CTE_HR/nep.2020" + month + ".nc"
        CTE_HR_data = nc.Dataset(file_path_CTE_HR)
        times_CTE_HR = CTE_HR_data.variables["time"]

    # VPRM:
    if VPRM_flag:
        file_path_VPRM = (
            "/media/madse/scratch/VPRM_data/VPRM_ECMWF_" + VPRM_var + "_2020_CP.nc"
        )
        VPRM_data = nc.Dataset(file_path_VPRM)
        times_VPRM = VPRM_data.variables["time"]

    # process CAMS data if times fit
    start_date_nc_f1 = datetime.strptime(start_date_str, "%Y-%m-%d_%H:%M:%S")
    new_time = start_date_nc_f1 - timedelta(minutes=timedelt)
    j = 0
    for time_CAMS in times_CAMS:
        date_CAMS = datetime(1900, 1, 1) + timedelta(hours=int(time_CAMS))
        j = j + 1
        if new_time == date_CAMS:
            lat_CAMS = CAMS_data.variables["latitude"][:]
            lon_CAMS = CAMS_data.variables["longitude"][:]
            var_CAMS = (
                CAMS_data.variables[CAMS_var][j - 1, :, :].data * factor
            )  # convert unit to mmol m-2 s-1
            CAMS_proj = proj_CAMS_on_WRF_grid(
                lat_CAMS,
                lon_CAMS,
                var_CAMS,
                lats_fine,
                lons_fine,
                WRF_var_d3,
            )

            CAMS_var_x_topo = np.ma.masked_where(~mask, CAMS_proj)
            CAMS_average_over_topo = np.mean(CAMS_var_x_topo)
        # CTE_HR: times_CTE_HR
    if CTE_HR_flag:
        j = 0
        for time_CTE_HR in times_CTE_HR:
            date_CTE_HR = datetime(2000, 1, 1) + timedelta(seconds=int(time_CTE_HR))
            j = j + 1
            if new_time == date_CTE_HR:
                lat_CTE_HR = CTE_HR_data.variables["latitude"][:]
                lon_CTE_HR = CTE_HR_data.variables["longitude"][:]
                var_CTE_HR = (
                    CTE_HR_data.variables["nep"][j - 1, :, :].data * 10**6
                )  # convert mol m-2 s-1 to mumol m-2 s-1
                CTE_HR_proj = proj_on_WRF_grid(
                    lat_CTE_HR,
                    lon_CTE_HR,
                    var_CTE_HR,
                    lats_fine,
                    lons_fine,
                    WRF_var_d3,
                )
                CTE_HR_var_x_topo = np.ma.masked_where(~mask, CTE_HR_proj)
                CTE_HR_average_over_topo = np.mean(CTE_HR_var_x_topo)
                print("projection of CTE_HR data")
                print("... at ", date_CTE_HR)
    else:
        CTE_HR_average_over_topo = np.nan

    if VPRM_flag:
        j = 0
        for time_VPRM in times_VPRM:
            date_VPRM = datetime(2020, 1, 1) + timedelta(hours=int(time_VPRM))
            j = j + 1
            if new_time == date_VPRM:
                lat_VPRM = VPRM_data.variables["lat"][:]
                lon_VPRM = VPRM_data.variables["lon"][:]
                var_VPRM = (
                    VPRM_data.variables[VPRM_var][j - 1, :, :].data * VPRM_factor
                )  # unit is mmol m-2 s-1
                VPRM_proj = proj_on_WRF_grid(
                    lat_VPRM,
                    lon_VPRM,
                    var_VPRM,
                    lats_fine,
                    lons_fine,
                    WRF_var_d3,
                )
                VPRM_var_x_topo = np.ma.masked_where(~mask, VPRM_proj)
                VPRM_average_over_topo = np.mean(VPRM_var_x_topo)
                print("projection of VPRM data")
                print("... at ", date_VPRM)
    else:
        VPRM_average_over_topo = np.nan

    time_step = nc_fid3.variables["Times"][:].tobytes().decode("utf-8")
    time_steps.append(time_step)
    i += 1
    # close the NetCDF files when done

    # Create a bar chart
    labels = [
        "WRF" + D03,
        "WRF " + D02,
        "WRF " + D01,
        "CAMS",
        "VPRM",
        "CTE_HR",
    ]
    values = [
        WRF_var_d3_topo_m,
        WRF_var_d2_topo,
        WRF_var_d1_topo,
        CAMS_average_over_topo,
        VPRM_average_over_topo,
        CTE_HR_average_over_topo,
    ]

    if plotting_refs:
        # remove dashes in timestr
        timestr = timestr.replace("-", "")

        figname = (
            "../plotting/means_of_WRF_CAMS_VPRM_SiB4_"
            + name_vars[WRF_var]
            + "_stdv_"
            + str(STD_VAL)
            + "_"
            + timestr
            + ".eps"
        )
        fig, ax = plt.subplots()
        bars = ax.bar(
            labels, values, color=["blue", "green", "red", "magenta", "orange", "brown"]
        )
        # Add labels to each bar
        for bar in bars:
            yval = bar.get_height()
            plt.text(
                bar.get_x() + bar.get_width() / 2,
                yval,
                round(yval, 2),
                ha="center",
                va="bottom",
            )
        plt.ylabel("Mean Values")
        plt.title("Interpol. on WRF d04 for " + name_vars[WRF_var])
        # set angle of labels to 45 °
        # plt.xticks(rotation=45)
        # plt.show()
        plt.savefig(figname, format="eps")
        plt.close()

    model_TSK_d3 = nc_fid3.variables["TSK"][0, :, :]

    model_TSK_d3_topo = np.ma.masked_where(~mask, model_TSK_d3)
    proj_model_TSK_d1 = proj_on_finer_WRF_grid(
        lats_d1, lons_d1, model_TSK_d1, lats_fine, lons_fine, model_TSK_d3
    )
    model_TSK_d1_topo = np.ma.masked_where(~mask, proj_model_TSK_d1)

    diff_TSK = model_TSK_d1_topo - model_TSK_d3_topo

    # scale by vegetation fraction to normalize for temp difference effect
    if WRF_var != "TSK":
        WRF_VEGFRA_d1 = nc_fid1.variables["VEGFRA"][0, :, :]
        WRF_var_d1[WRF_VEGFRA_d1 < VEGFRA_percentage] = np.nan
        WRF_var_d1 = WRF_var_d1 * WRF_VEGFRA_d1 / 100

    proj_WRF_var_d1 = proj_on_finer_WRF_grid(
        lats_d1, lons_d1, WRF_var_d1, lats_fine, lons_fine, WRF_var_d3
    )

    if WRF_var != "TSK":
        WRF_VEGFRA_d3 = nc_fid3.variables["VEGFRA"][0, :, :]
        WRF_var_d3[WRF_VEGFRA_d3 < VEGFRA_percentage] = np.nan
        WRF_var_d3 = WRF_var_d3 * WRF_VEGFRA_d3 / 100

    diff_var = np.ma.masked_where(~mask, proj_WRF_var_d1) - np.ma.masked_where(
        ~mask, WRF_var_d3
    )

    idx = np.isfinite(diff_var) & np.isfinite(diff_TSK)

    coeff = np.polyfit(diff_TSK[idx], diff_var[idx], deg=1)
    x_poly = np.linspace(diff_TSK[idx].min(), diff_TSK[idx].max())
    y_poly = np.polyval(coeff, x_poly)
    if plotting_scatter:
        fig, ax = plt.subplots()
        ax.scatter(diff_TSK[idx], diff_var[idx], s=0.2, c="k")
        # Plot regression line
        a, b = coeff
        ax.plot(
            x_poly,
            y_poly,
            color="b",
            lw=1.5,
            linestyle="--",
            label=f"y = {a:.2f} * x + {b:.2f}",
        )
        ax.legend()
        t1 = "Mean differences: %s K and %s m" % (
            "{:.2f}".format(diff_var[idx].mean()),
            "{:.2f}".format(diff_TSK[idx].mean()),
        )
        plt.text(0, 0, t1, ha="left", wrap=True)
        ax.xaxis.grid(True, which="major")
        ax.yaxis.grid(True, which="major")
        ax.set_xlabel("T diff [K]")
        ax.set_ylabel(f"{name_vars[WRF_var]} diff")
        plt.title(
            "WRF d02 - d04 TSK and %s correlation %s" % (name_vars[WRF_var], timestr)
        )
        figname = "../plotting/WRF_d02_d04_TSK_%s_correlation_%s.eps" % (
            WRF_var,
            timestr,
        )
        # plt.show()
        # plt.close()
        nc_fid3.close()
        plt.savefig(figname, format="eps")

    coeff_all_T = []
    coeff_all2_T = []
    # select range of T_refs
    T_ref_values = range(
        int(model_TSK_d1_topo.min()), int(model_TSK_d1_topo.max()), T_bin_size
    )

    if flag_ini:
        df_coeff = pd.DataFrame(index=T_ref_values)
        flag_ini = False

    if T_bin_flag:
        # Create a mask for model_TSK_d3_topo between 300 and 301 K

        for T_ref in T_ref_values:
            try:
                temp_mask = (model_TSK_d1_topo >= T_ref) & (
                    model_TSK_d1_topo <= T_ref + T_bin_size
                )

                # Apply the mask to differences
                masked_diff_TSK = diff_TSK[temp_mask]
                masked_diff_var = diff_var[temp_mask]

                idx = np.isfinite(masked_diff_var) & np.isfinite(masked_diff_TSK)

                # Apply the overall mask
                diff_TSK_t = masked_diff_TSK[idx]
                diff_var_t = masked_diff_var[idx]

                # Make a writable version of the arrays (if they are masked)
                diff_TSK_t = np.array(diff_TSK_t)
                diff_var_t = np.array(diff_var_t)

                # Identify and exclude outliers using the IQR method
                Q1_diff_TSK = np.percentile(diff_TSK_t, 25)
                Q3_diff_TSK = np.percentile(diff_TSK_t, 75)
                IQR_diff_TSK = Q3_diff_TSK - Q1_diff_TSK
                lower_bound_diff_TSK = Q1_diff_TSK - 1.5 * IQR_diff_TSK
                upper_bound_diff_TSK = Q3_diff_TSK + 1.5 * IQR_diff_TSK

                Q1_diff_var = np.percentile(diff_var_t, 25)
                Q3_diff_var = np.percentile(diff_var_t, 75)
                IQR_diff_var = Q3_diff_var - Q1_diff_var
                lower_bound_diff_var = Q1_diff_var - 1.5 * IQR_diff_var
                upper_bound_diff_var = Q3_diff_var + 1.5 * IQR_diff_var

                # Create masks to exclude outliers
                outlier_mask = (
                    (diff_TSK_t >= lower_bound_diff_TSK)
                    & (diff_TSK_t <= upper_bound_diff_TSK)
                    & (diff_var_t >= lower_bound_diff_var)
                    & (diff_var_t <= upper_bound_diff_var)
                )

                # Apply the mask
                diff_TSK_no_outliers = diff_TSK_t[outlier_mask]
                diff_var_no_outliers = diff_var_t[outlier_mask]
                coeff = np.polyfit(masked_diff_TSK[idx], masked_diff_var[idx], deg=1)
                coeff2 = np.polyfit(diff_TSK_no_outliers, diff_var_no_outliers, deg=1)
                a, b = coeff
                a2, b2 = coeff2
                if plotting_scatter_all:
                    fig, ax = plt.subplots()
                    ax.scatter(
                        masked_diff_TSK[idx], masked_diff_var[idx], s=0.1, c="red"
                    )
                    x_poly = np.linspace(
                        masked_diff_TSK[idx].min(), masked_diff_TSK[idx].max()
                    )
                    y_poly = np.polyval(coeff, x_poly)
                    ax.plot(
                        x_poly,
                        y_poly,
                        color="b",
                        lw=1.5,
                        linestyle="--",
                        label=f"y_all = {a:.2f} * x + {b:.2f}",
                    )
                    # t1 = "Mean differences: %s K and %s m" % (
                    #     "{:.2f}".format(masked_diff_var[idx].mean()),
                    #     "{:.2f}".format(masked_diff_TSK[idx].mean()),
                    # )
                    # plt.text(0, 900, t1, ha="left", wrap=True)

                    # ax.scatter(diff_TSK_no_outliers, diff_var_no_outliers, s=0.2, c="k")
                    # x_poly_b = np.linspace(diff_TSK_no_outliers.min(), diff_TSK_no_outliers.max())
                    # y_poly_b = np.polyval(coeff2, x_poly_b)
                    # ax.set_xlim(-7.5, 7.5)
                    # ax.set_ylim(-10, 10)
                    # ax.plot(
                    #     x_poly_b,
                    #     y_poly_b,
                    #     color="k",
                    #     lw=1.5,
                    #     linestyle="-.",
                    #     label=f"y_perc = {a2:.2f} * x + {b2:.2f}",
                    # )
                    ax.legend()

                    ax.xaxis.grid(True, which="major")
                    ax.yaxis.grid(True, which="major")
                    ax.set_xlabel("TSK diff [K]")
                    ax.set_ylabel(f"{name_vars[WRF_var]} diff")
                    plt.title(
                        "WRF d02 - d04 TSK and %s correlation at %s T_ref"
                        % (name_vars[WRF_var], T_ref)
                    )
                    figname = (
                        "../plotting/WRF_d02_d04_TSK_%s_correlation_%s_T_ref.eps"
                        % (
                            WRF_var,
                            T_ref,
                        )
                    )
                    # plt.show()
                    plt.savefig(figname, format="eps")
                # plt.show()
                # plt.close()
                coeff_all_T.append(a)
                coeff_all2_T.append(a2)
            except:
                print("Not enough Data for T_ref=%s" % T_ref)
                coeff_all_T.append(np.nan)

    df_coeff[name_vars[WRF_var]] = coeff_all_T
    # df_coeff[WRF_var + "_perc"] = coeff_all2_T
if T_bin_flag:
    # Plotting
    ax = df_coeff.plot(marker="o", linestyle="-", figsize=(10, 6), grid=True)

    # Adding labels and title
    ax.set_xlabel("T_ref")
    ax.set_ylabel("$\\beta$ coefficient Values")
    ax.set_title("$\\beta$ Coefficient Values for NEE, GPP, and RECO")
    figname = "../plotting/WRF_d02_d04_T_ref_coefficients_%s_STD_%s.eps" % (
        timestr,
        STD_VAL,
    )
    plt.savefig(figname, format="eps")
    # Show the plot
    # plt.show()
    print(df_coeff)

    # # Create a contour plot
    # fig, ax = plt.subplots()
    # contour_plot = ax.contourf(diff_var[0, :, :], cmap="viridis")

    # # Add colorbar
    # cbar = plt.colorbar(contour_plot, ax=ax)
    # cbar.set_label("[Your Colorbar Label]")

    # # Add labels and title
    # ax.set_xlabel("[Your X-axis Label]")
    # ax.set_ylabel("[Your Y-axis Label]")
    # plt.title("Contour Plot of diff_var")

    # # Show the plot
    # plt.show()
